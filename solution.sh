#!/bin/bash

# Copyright (c) 2017 Thomas Foerster. All rights reserved.

# finding wanted search key line number
init1=$(grep -n '@' $2 | grep '{'$1',' | awk -F: '{print $1}')
last1=$(wc -l $2 | awk '{print $1}')

tmp=$(echo "tmp_$2")
tmp2=$(echo "tmp2_$2")

# Removing temporal file
if [ -f $tmp ]
then
    rm $tmp
    rm $tmp2
fi


# Starting from one line below the searched document for extracting the bib information
init1=`expr $init1 + 1`


# Extracting bib information of the one given key
while (( $init1 < $last1 ))
do
    ttt=$(sed -n ''$init1'p' $2) 
    echo $ttt >> $tmp
    if [[ "${ttt:0:1}" = "@" ]]
    then
       break
    fi
    init1=`expr $init1 + 1`
done


# Extracting of author, journal, and year in the right format

## Author entry
author=$(grep -i -e 'author.*=' $tmp | \
    awk -F= '{print $2}' | \
    sed 's/{//g' | \
    sed 's/},//g' | \
    sed 's/^ *//' | \
    sed 's/ and /\n/gI' | \
    awk -F, '{print $2" " $1 ","}')


if [ "$author" = "" ]
then
    author=$(grep -i -e 'editor.*=' $tmp | \
    awk -F= '{print $2}' | \
    sed 's/{//g' | \
    sed 's/},//g' | \
    sed 's/^ *//' | \
    sed 's/ and /\n/gI' | \
    awk -F, '{print $2 " " $1 ","}')
fi
#-- Note for awk: with substr($2,0,2) you can extract first two characters

printf '%s\n' "$author" > $tmp2

printf '%s\n' "$author" | while read -r line
do
    n_col=$(echo $line | awk '{print NF}')

    for (( iii=1; iii<n_col; iii++ )) 
    do 
        first1=$(echo $line | awk '{print $'$iii'}')
        
        # handler for first names with - seperator
        if echo $line | awk '{print $'$iii'}' | grep -q '-'
        then
            first2=$(echo $line |\
                awk '{print $'$iii'}' |\
                sed 's/^ *\([A-Z]\)[a-z]\+/\1\./' |\
                awk -F- '{print $1"-"substr($2,0,1)"."}')
        else
            first2=$(echo $line | awk '{print $'$iii'}' | sed 's/^ *\([A-Z]\)[a-z]\+/\1\./')
        fi

        line2=$(echo $line | sed "s/$first1/$first2/")
        author=$(echo $author | sed "s/$line/$line2/")
        echo $author | sed "s/$line/$line2/" > $tmp2
    done
done

author=$(cat $tmp2| \
    sed 's/[a-Z]\./& /g' |\
    sed -r 's/([A-Z][a-z]+)[.;:]+ /\1/g' |\
    sed -r 's/([[:lower:]])\. /\U\1\. /g' |\
    sed -r 's/\<([A-Z]) /\1\. /g' )
#|\
#    sed -E 's/([A-Z][a-z]+)[.;:]+ /\1/g' )



if [ "$author" = "" ]
then
    author="Unknown author"
fi


## Journal entry
journal1=$(grep -i journal $tmp | \
    awk -F= '{print $2}' | \
    sed 's/{//g' | \
    sed 's/},//g' | \
    sed 's/^ *//' | \
    sed 's/ *$//' | \
    sed 's/}//g' | \
    sed 's/ \\\& / \& /g')

journal2=$(grep "$journal1;" $3 | \
    awk -F\; '{print $2}' | \
    sed 's/^ *//'i | \
    sed 's/ *$//')

if [ "$journal2" = "" ]
then
    journal=$journal1
else
    journal=$journal2
fi

if [ "$journal1" = "" ]
then
    journal=""
fi

## Year entry
year=$(grep -i year $tmp | \
    awk -F= '{print $2}' | \
    sed 's/{//g' | \
    sed 's/},//g' | \
    sed 's/^ *//' | \
    sed 's/ *$//' | \
    sed -r 's/^[0-9]+/(&)/')

if [ "$year" = "" ]
then
    year=$(grep -i 'date.*=' $tmp | \
    awk -F= '{print $2}' | \
    awk -F- '{print $1}' | \
    sed 's/{//g' | \
    sed 's/},//g' | \
    sed 's/^ *//' | \
    sed 's/ *$//' | \
    sed -r 's/^[0-9]+/(&)/')
fi

if [ "$year" = "" ]
then
    year=""
fi


# Case handling if key do not exist
if [ "$author" = "Unknown author" -a "$journal" = "" -a "$year" = "" ]
then
    echo "Entry '$1' not found in '$2'"
else
    echo $author $journal $year | \
    sed 's/ \{1,\}/ /g'
fi


# Clean up
rm $tmp
rm $tmp2
